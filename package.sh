#!/usr/bin/env bash

ME=$(basename "$0")

usage() {
  echo "Usage: $ME" >&2
  exit 1

}

helm package web
helm package pgweb
helm package next
helm package next-graphql
helm package cockroach-changefeed-app
helm package confluent-prometheus-bridge
helm package taskforce
helm package eks-nvme-provisioner
helm package mryum-cockroachdb

helm repo index ./ --url https://gitlab.com/mr-yum/charts/-/raw/master
